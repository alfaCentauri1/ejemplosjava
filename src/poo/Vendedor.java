package poo;

public class Vendedor extends Persona{
    private float salarioHora;
    private float horasTrabajo;
    public static int contador;
    public Vendedor(String nombre, String apellido, short edad, int altura) {
        super(nombre, apellido, edad, altura);
        Vendedor.contador++;
    }

    public int modificarEdad(short valor){
        return (this.edad - valor);
    }

    public float getSalarioHora() {
        return salarioHora;
    }

    public void setSalarioHora(float salarioHora) {
        this.salarioHora = salarioHora;
    }

    public float getHorasTrabajo() {
        return horasTrabajo;
    }

    public void setHorasTrabajo(float horasTrabajo) {
        this.horasTrabajo = horasTrabajo;
    }
}
