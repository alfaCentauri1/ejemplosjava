package poo;

public class Ejecutable {
    public static void main(String args[]){
        Persona persona1 = new Persona("Pedro", "Perez", (short) 25, 180);
        System.out.println("Ejemplo de uso de métodos de la clase.");
        System.out.println("Nombre: " + persona1.getNombre());
        System.out.println("Apellido: " + persona1.getApellido());
        System.out.println("Edad: " + persona1.edad);
        System.out.println("Altura en cm: " + persona1.altura);
        System.out.println();
        //Clase hija
        System.out.println("Ejemplo de uso de herencia.");
        Vendedor vendedor1 = new Vendedor("Maria", "Lopez", (short) 30, 160);
        System.out.println("Nombre: " + vendedor1.getNombre());
        System.out.println("Apellido: " + vendedor1.getApellido());
        System.out.println("Edad: " + vendedor1.edad);
        System.out.println("Altura en cm: " + vendedor1.altura);
        System.out.println("Edad modificada: " + vendedor1.modificarEdad((short) 2));
        System.out.println("Contador de vendedores: " + Vendedor.contador);
        System.out.println();
        //
        //Con interfaces
        System.out.println("Ejemplo de uso de interfaces.");
        Cajero cajero = new Cajero("Jose", "Perez", (short)39, 182);
        System.out.println("Nombre: " + cajero.getNombre());
        System.out.println("Apellido: " + cajero.getApellido());
        System.out.println("Edad: " + cajero.getEdad());
        System.out.println("Altura en cm: " + cajero.altura);
        System.out.println("Contador de vendedores: " + Vendedor.contador);
        System.out.println(cajero.imprimirCargo());
        cajero.setHorasTrabajo(12f);
        cajero.setSalarioHora(4.5f);
        cajero.setPrecioHorasExtras(5.1f);
        System.out.println("El costo de la hora extra: " + cajero.getPrecioHorasExtras());
        cajero.imprimirHorasExtras();
        System.out.println();
        //
        Cajero cajero2 = new Cajero("Carlos", "Perez", (short)39, 182);
        System.out.println("Nombre: " + cajero2.getNombre());
        System.out.println("Apellido: " + cajero2.getApellido());
        System.out.println("Edad: " + cajero2.getEdad());
        System.out.println("Altura en cm: " + cajero2.altura);
        System.out.println("Contador de vendedores: " + Vendedor.contador);
        System.out.println(cajero2.imprimirCargo());
        cajero2.setHorasTrabajo(12f);
        cajero2.setSalarioHora(4.5f);
        cajero2.setPrecioHorasExtras(4.9f);
        System.out.println("El costo de la hora extra: " + cajero2.getPrecioHorasExtras());
        cajero2.imprimirHorasExtras();
    }
}
