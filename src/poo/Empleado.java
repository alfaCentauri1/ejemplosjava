package poo;

public interface Empleado {
    public String imprimirCargo();
    public void calcularSalario();
    public float calcularHorasExtras(float horas);
    public void imprimirHorasExtras();
    public void imprimirHorasTrabajadas();
}
