package poo;

public class Cajero extends Vendedor implements Empleado{
    private float salario;
    private float precioHorasExtras;

    private float horasExtras;

    public Cajero(String nombre, String apellido, short edad, int altura) {
        super(nombre, apellido, edad, altura);
    }

    @Override
    public String imprimirCargo() {
        return "El empleado " + this.getNombre() + " es un cajero.";
    }

    @Override
    public void calcularSalario() {
        salario = this.getSalarioHora() * this.getHorasTrabajo();
    }

    @Override
    public float calcularHorasExtras(float horas) {
        horasExtras = (this.getHorasTrabajo() - 8) * precioHorasExtras;
        return horasExtras;
    }

    @Override
    public void imprimirHorasExtras() {
        System.out.println("El pago de las horas extras del vendedor " + this.getNombre() + " " + this.getApellido() +
                " son: " + calcularHorasExtras(horasExtras) + "$");
    }

    @Override
    public void imprimirHorasTrabajadas() {

    }

    public float getSalario() {
        return salario;
    }

    public float getPrecioHorasExtras() {
        return precioHorasExtras;
    }

    public void setPrecioHorasExtras(float precioHorasExtras) {
        this.precioHorasExtras = precioHorasExtras;
    }
}
