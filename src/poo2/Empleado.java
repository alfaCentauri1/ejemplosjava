package poo2;

public class Empleado
{
    private String primerNombre;
    private String apellidoPaterno;
    private static int cuenta = 0; // número de objetos en memoria

    public Empleado( String nombre, String apellido )
    {
        primerNombre = nombre;
        apellidoPaterno = apellido;
        cuenta++; // incrementa la variable static cuenta de empleados
        System.out.printf( "Constructor de Empleado: %s %s; cuenta = %d\n",
                primerNombre, apellidoPaterno, cuenta );
    }

    protected void ﬁnalize()
    {
        cuenta--; // decrementa la variable static cuenta de empleados
        System.out.printf( "Finalizador de Empleado: %s %s; cuenta = %d\n",
                primerNombre, apellidoPaterno, cuenta );
    }

    public String obtenerPrimerNombre()
    {
        return primerNombre;
    }

    public String obtenerApellidoPaterno()
    {
        return apellidoPaterno;
    }

    public static int obtenerCuenta()
    {
        return cuenta;
    }
}