package main;

public class Ciclos {
    public static void main(String args[]){
        long a = 0, b = 1, fibonacci = 0;
        System.out.println("Ejercicio de ciclos: Serie de Fibonacci.");
        System.out.println("Serie Fibonacci: ");
        System.out.print( a + ", " + b);
        for(var i = 2; i < 100; i++){
            fibonacci = a + b;
            if(fibonacci > 0) {
                a = b;
                b = fibonacci;
                System.out.print(", ");
                if (i % 10 == 0)
                    System.out.println();

                System.out.print(fibonacci);
            }
            else{
                break; // i=100; /* Es mejor que el break */
            }
        }
        System.out.println();
    }
}
