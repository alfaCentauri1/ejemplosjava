package main;

public class TiposVariables {
    public static void main(String args[]){
        byte numeroByte = 18;
        System.out.println("Ejemplos de tipos de variables enteras.");
        System.out.println("numeroByte = " + numeroByte);
        System.out.println("Valor mínimo del tipo Byte = " + Byte.MIN_VALUE);
        System.out.println("Valor máximo del tipo Byte = " + Byte.MAX_VALUE);

        short numeroShort = 32123;
        System.out.println("numeroShort = " + numeroShort);
        System.out.println("Valor mínimo del tipo Short = " + Short.MIN_VALUE);
        System.out.println("Valor máximo del tipo Short = " + Short.MAX_VALUE);

        int numeroInt = 2032123;
        System.out.println("numeroShort = " + numeroInt);
        System.out.println("Valor mínimo del tipo Integer = " + Integer.MIN_VALUE);
        System.out.println("Valor máximo del tipo Integer = " + Integer.MAX_VALUE);

        long numeroLong = 18123456789L;
        System.out.println("numeroShort = " + numeroLong);
        System.out.println("Valor mínimo del tipo Long = " + Long.MIN_VALUE);
        System.out.println("Valor máximo del tipo Long = " + Long.MAX_VALUE);
    }
}
