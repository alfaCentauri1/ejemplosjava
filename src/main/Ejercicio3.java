package main;

import java.util.Scanner;

public class Ejercicio3 {
    public static void main(String args[]){
        System.out.println("Calcular area y perímetro de un rectangulo.");
        int area = 0, perimetro = 0;
        var consola = new Scanner(System.in);
        int base = 0, altura = 0;
        System.out.println("Indique la base del rectangulo.");
        base = consola.nextInt();
        System.out.println("Indique la altura del rectangulo.");
        altura = consola.nextInt();
        area = base * altura;
        perimetro = (base + altura) * 2;
        System.out.println("El area es " + area);
        System.out.println("El perimetro es " + perimetro);
    }
}
