package main;

public class VariableFlotantes {
    public static void main(String args[]) {
        float numeroFlotante = (float) 2.4567;
        System.out.println("Ejemplos de tipos de variables folat.");
        System.out.println("numeroShort = " + numeroFlotante);
        System.out.println("Valor mínimo del tipo Float = " + Float.MIN_VALUE);
        System.out.println("Valor máximo del tipo Float = " + Float.MAX_VALUE);

        double numeroDouble = 3.141516D;
        System.out.println("numeroDouble = " + numeroDouble);
        System.out.println("Valor mínimo del tipo Double = " + Double.MIN_VALUE);
        System.out.println("Valor máximo del tipo Double = " + Double.MAX_VALUE);
    }
}
