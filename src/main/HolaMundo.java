package main;

public class HolaMundo {
    public static void main(String args[]){
        System.out.println("Hola mundo desde Java 18");
        int pi = 3;
        System.out.printf("El valor entero es %d\n", pi);
        float numeroPi = (float) 3.141516;
        System.out.printf("El valor del número pi es %f\n", numeroPi);
        //Inferencia de tipos de variables
        var variableInferida = "Esto es una prueba de la palabra reservada var";
        System.out.println(variableInferida);
    }
}
